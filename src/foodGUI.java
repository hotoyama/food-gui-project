import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class foodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane OrderedItem;
    private JButton pizaButton;
    private JButton hamburgerButton;
    private JButton sushiButton;
    private JButton checkOutButton;
    private JLabel totalcost;
    private JTabbedPane tabbedPane1;
    private JButton colaButton;
    private JButton potatoButton;
    private JButton icecreamButton;
    private JButton karaageButton;


    int sum = 0;
//    void order(String foodname){
//            int confirmation = JOptionPane.showConfirmDialog(null,
//                    "Would you like to order "+foodname+"?",
//                    "Order Confirmation",
//                    JOptionPane.YES_NO_OPTION);
//            if (confirmation == 0) {
//                JOptionPane.showMessageDialog(null,"Order for "+foodname+" received");
//                OrderedItem.setText("Order for "+foodname+" received");
//            }
//    }


    void order(String food, int cost){
        int confirmation=JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering "+food+"! It will be served as soon as possible.");
            String currentText1=OrderedItem.getText();
            OrderedItem.setText(currentText1+food+"     "+cost+"yen\n");
            sum+=cost;
            totalcost.setText("Total "+sum+"yen.\n");
        }
    }

    public foodGUI() {
        totalcost.setText("Total "+sum+"yen.");

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",700);
            }
        });
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("tempura1.jpg")));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                order("Ramen",600);
            }
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen1.jpg")));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",300);
            }
        });
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("udon1.jpg")));

        pizaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("piza",800);
            }
        });
        pizaButton.setIcon(new ImageIcon(this.getClass().getResource("piza01.jpg")));

        hamburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("hamburger",500);
            }
        });
        hamburgerButton.setIcon(new ImageIcon(this.getClass().getResource("hamburger1.jpg")));

        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("sushi",1000);
            }
        });
        sushiButton.setIcon(new ImageIcon(this.getClass().getResource("sushi1.jpg")));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,
                        "Would you like to CheckOut?",
                        "CheckOut Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you for ordering Food! The total cost is "+sum+"yen.");
                    OrderedItem.setText("");
                    sum=0;
                    totalcost.setText("Total "+sum+"yen.");
                }
            }
        });
        colaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("cola",150);
            }
        });
        potatoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("potato",250);
            }
        });
        icecreamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("icecream",200);
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("karaage",400);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodGUI");
        frame.setContentPane(new foodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
